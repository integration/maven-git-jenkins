package com.blogspothellohlassfishmaven;

import javax.ejb.Singleton;

@Singleton
public class NameStorageBean {

    // name field
    private String name = "GlassFish Server";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
